**JEU DU JUSTE PRIX**

![Texte alternatif](http://2.bp.blogspot.com/-OeY56IX3WeQ/U51yJByMmSI/AAAAAAAAA2s/G3AEy_HrJDA/s1600/juste+prix.jpg)
```php

#1 Les importation
import sys
import random   
```
Tout d'abord pour commencer le programme, j'ai importé des modules qui me serviront au cours du jeu. 
Par exemple "random", il sert à choisir un nombre aléatoire,
et "sys" sert à arrêter un programme".

```php
#2 Les fonctions
def jeu():
    joueur = (input("Nom du joueur :"))     
    nombre_choisi = random.randint(0,100)
    vies = 10
  
```
Ensuite j'ai créé une fonction appelée jeu (qui ne prend en compte aucun argument), 
ou je demande le nom du joueur, 
fait choisir un nombre aléeatoir par le module random,
et defini le nombre de vie du joueur.

```php
  jouer = (input('êtes vous prêt à jouer ? oui ou non '))    
    if jouer == 'oui':
        commencer = True
    else :
        sys.exit()
```

Dans cette même fonction, je demande au joueur si il est prêt à jouer.
Si il répond "oui", sa réponse qui est stocké dans la variable 'joué' renverra "True" et le programme principal commencera.
Si il répond "non", sa réponse qui est stocké dans la variable 'joué' renverra "False" et le programme sera arrêté

```php
#3 Le programme principal
while commencer and vies >= 0  :                                                       
        nombre_rentre = input("entrez un nombre ")                          
        nombre_rentre = int(nombre_rentre) 
```
Après, le programme pricipale se lance si il répond "oui".
J'ai commencé par créér une boucle qui s'éxécutera tant que :
"commencer" sera égal à "True"
le nombre de vies est supérieur à 0.

```php
if nombre_rentre < nombre_choisi :                                 
            commentaire = " nombre trop bas"                               
            print(commentaire.upper(), "\n plus que", vies, "vies"  )      
            vies-=1  
```
Dans cette boucle,
Je commence avec la condition suivante: "if"
si le nombre rentré par le joueur est inférieur au nombre choisi par le module random,
il affiche le "commentaire" "nombre trop bas",
et il retirer 1 vie.
J'ai affecté ma chaine de caractère à la variable "commentaire"
J'ai utilisé ".upper()" qui me permet de mettre la chaine de caractère en majuscule,
ainsi que ""\n" qui me permet de mettre ma chaine de caractère à la ligne.

```php
elif nombre_rentre > nombre_choisi :                               
            commentaire = " nombre trop haut"                               
            print(commentaire.upper(), "\n plus que", vies, "vies")         
            vies-=1    
```
Je poursuis avec une autre condition: "elif"
si le nombre rentré par le joueur est supérieur au nombre choisi par le module random,
il affiche le "commentaire" "nombre trop haut",
et il retire 1 vie.
J'ai affecté ma chaine de caractère à la variable "commentaire"
J'ai utilisé ".upper()" qui me permet de mettre la chaine de caractère en majuscule,
ainsi que ""\n" qui me permet de mettre ma chaine de caractère à la ligne.

```php
elif nombre_rentre == nombre_choisi :                             
            print("bravo ! vous avez gagné !") 
```
J'utilise la même condition que la précédente: "elif"
si le nombre rentré par le joueur est égal au nombre choisi par le module random,
La chaine de caractère "bravo! vous avez gagné" sera affichée.

```php
   else :                                                               
        print('plus de vie, vous avez perdu !' '\n si vous voulez rejouer relancer le programme')     
```
Je change de condition et utilise: "else"
si toutes les conditions précédentes ne sont pas remplies 
alors afficher la chaîne de caractère "plus de vie, vous avez perdu !' '\n si vous voulez rejouer relancer le programme"

```php
jeu() 
```
Pour terminer ce programme, j'appelle la fonction de départ qui est "jeu()".
Elle permettra au programme de l'éxécuter lors de son lancement.





